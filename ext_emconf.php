<?php

$EM_CONF[$_EXTKEY] = array(
    'title' => 'Unroll TYPO3 save buttons',
    'description' => 'Shows the save buttons in the TYPO3 BE next to each other instead of the dropdown.',
    'category' => 'plugin',
    'version' => '1.0.0',
    'state' => 'stable',
    'uploadfolder' => false,
    'author' => 'Markus Klein',
    'author_email' => 'support@reelworx.at',
    'author_company' => 'Reelworx GmbH',
    'constraints' => array(
        'depends' => array(
            'php' => '5.5.0-0.0.0',
            'typo3' => '7.6.2-8.0.99',
        ),
        'conflicts' => array(),
        'suggests' => array(),
    ),
);
